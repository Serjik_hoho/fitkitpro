package com.gym.rvAdapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gym.R;
import com.gym.models.WorkoutsByCoach;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jagor on 9/17/2017.
 */

public class RVAdapterWorkoutsByCoach extends RecyclerView.Adapter<RVAdapterWorkoutsByCoach.ViewHolderWorkoutByCoach>  {

    private ArrayList<WorkoutsByCoach> workouts;
    private Context context;
    private WorkoutsByCoach workoutsByCoach;

    public RVAdapterWorkoutsByCoach(ArrayList<WorkoutsByCoach> workouts, Context context) {
        this.workouts = workouts;
        this.context = context;
    }

    @Override
    public ViewHolderWorkoutByCoach onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.workouts_by_coach_list_item, parent, false);
        ViewHolderWorkoutByCoach viewHolderWorkoutByCoach = new ViewHolderWorkoutByCoach(view);
        return viewHolderWorkoutByCoach;
    }

    @Override
    public void onBindViewHolder(ViewHolderWorkoutByCoach holder, int position) {
        workoutsByCoach = workouts.get(position);

        holder.itemWorkoutTitle.setText(workoutsByCoach.getTitle());
        holder.itemNoteTxt.setText(workoutsByCoach.getNote());
        holder.itemCoachName.setText(workoutsByCoach.getCoachName());
        holder.itemCoachAvatar.setImageResource(R.drawable.ilya);
    }

    @Override
    public int getItemCount() {
        return workouts.size();
    }

    class ViewHolderWorkoutByCoach extends RecyclerView.ViewHolder {

        TextView itemWorkoutTitle, itemNoteTxt, itemCoachName;
        ImageView itemCoachAvatar;

        public ViewHolderWorkoutByCoach(View itemView) {
            super(itemView);

            itemWorkoutTitle = (TextView) itemView.findViewById(R.id.item_workout_title);
            itemNoteTxt = (TextView) itemView.findViewById(R.id.item_note_txt);
            itemCoachName = (TextView) itemView.findViewById(R.id.item_coach_name);
            itemCoachAvatar = (ImageView) itemView.findViewById(R.id.item_coach_avatar);
        }
    }
}
