package com.gym;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.gym.Abstract.AbstractFragment;
import com.gym.api.RetrofitApi;
import com.gym.api.RetrofitListener;
import com.gym.api.RetrofitWorker;
import com.gym.fragments.Exercises;
import com.gym.fragments.Feed;
import com.gym.fragments.More;
import com.gym.fragments.Workouts;

public class MainActivity extends ParentActivity{



    //Listeners
    BottomNavigationView.OnNavigationItemSelectedListener BNVListener= new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_feed:
                    Toast.makeText(MainActivity.this, "Feed", Toast.LENGTH_SHORT).show();
                    showFragment(new Feed());
                    Log.d("MY_TAG", Feed.class.getName());
                    break;
                case R.id.action_workouts:
                    Toast.makeText(MainActivity.this, "Workout", Toast.LENGTH_SHORT).show();
                    showFragment(new Workouts());
                    break;
                case R.id.action_exercised:
                    Toast.makeText(MainActivity.this, "Exercise", Toast.LENGTH_SHORT).show();
                    showFragment(new Exercises());
                    break;
                case R.id.action_more:
                    Toast.makeText(MainActivity.this, "More", Toast.LENGTH_SHORT).show();
                    showFragment(new More());
                    break;
            }
            return true;
        }
    };



    //Fields
    BottomNavigationView bottomMenu;

    RetrofitApi retrofitApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomMenu = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomMenu.setOnNavigationItemSelectedListener(BNVListener);
        showFragment(new Feed());

        retrofitApi = new RetrofitApi();
    }

   public void showFragment(AbstractFragment fragment){
        openFragment(fragment,R.id.main_container);

        switch (fragment.getClass().getName()){
            case "com.gym.fragments.Feed":
                setTitle("Feed");
                break;
            case "com.gym.fragments.Workouts":
                setTitle("Workouts");
                break;
            case "com.gym.fragments.Exercises":
                setTitle("Exercise");
                break;
            case "com.gym.fragments.More":
                setTitle("More");
                break;
        }
    }



    public void checkApi(View view){
        JsonObject object = new JsonObject();
        object.addProperty("firstName","Pervonax");
        object.addProperty("lastName","Kokoko");
        object.addProperty("email","alomchiki");
        object.addProperty("password","12345");

        retrofitApi.registrationUser(object, new RetrofitListener<JsonObject>() {
            @Override
            public void onResponse(JsonObject data) {
                Log.i("CatchResponse",data.toString());
            }

            @Override
            public void onError(Throwable t) {
                Log.i("CatchResponse","NE OCHEN---" + t.toString());
            }
        });}
}
