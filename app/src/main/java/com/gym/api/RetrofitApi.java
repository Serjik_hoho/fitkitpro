package com.gym.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Serj on 14.09.2017.
 */

public class RetrofitApi {
    private static String BASE_URL = "https://fitkitapp.herokuapp.com/";
    private static Retrofit retrofit;
    private static String CONTENT_TYPE = "application/json";
    private final Gson gson;
    private static String token;


    public RetrofitApi() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    private RequestBody createBody(JsonObject input){
        return RequestBody.create(MediaType.parse("application/json"),
                gson.toJson(input));
    }


    public void registrationUser(JsonObject params,RetrofitListener<JsonObject>callback){
        retrofit.create(SendApi.class).loadRepo(CONTENT_TYPE,createBody(params)).enqueue(new RetrofitWorker<JsonObject>(callback) {
        });

    }

    public interface SendApi {
        @POST("client/register")
        Call<ResponseBody> loadRepo(@Header("Content-Type") String content_type, @Body RequestBody send);
    }
//    public interface SendApi {
//        @POST("{client/registration}")
//        Call<ResponseBody> loadRepo(@Header("Content-Type") String content_type, @Path("path") String path, @Body RequestBody send);
//    }
}
