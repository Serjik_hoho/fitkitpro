package com.gym.api;

import com.google.gson.JsonObject;

/**
 * Created by Serj on 03.10.2017.
 */

public interface RetrofitListener<V> {
    void onResponse(JsonObject data); //Response
    void onError(Throwable t); //Error
}
