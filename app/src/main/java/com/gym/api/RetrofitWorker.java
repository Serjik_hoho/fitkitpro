package com.gym.api;


import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Serj on 14.09.2017.
 */

public abstract class RetrofitWorker<V> implements Callback<ResponseBody> {
    private RetrofitListener callback;

    protected RetrofitWorker(RetrofitListener<V> callback) {
        this.callback = callback;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.body() == null) {
            callback.onError(new Throwable("Server is down"));
        }

        try {
            Log.i("SearchResp",String.valueOf(response.code())+"---");
             Log.i("SearchResp","string"+response.body().string());
        } catch (IOException e) {
            Log.i("SearchResp","Fail");
            e.printStackTrace();
        }
        Log.i("SearchResp","string"+response.body().toString());
        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject result;
            String response_string = response.body().string();
            result = (JsonObject) jsonParser.parse(response_string);
            if (result != null)
                callback.onResponse(result);
            else
                callback.onError(new Throwable("Something wrong, ask Boris"));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable throwable) {

    }
}
