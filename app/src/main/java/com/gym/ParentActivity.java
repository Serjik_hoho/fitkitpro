package com.gym;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.gym.Abstract.AbstractFragment;
import com.gym.controllers.GlobalValues;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serj on 04.09.2017.
 */

public class ParentActivity extends AppCompatActivity {
    public String currentFragmentTag;
    public AbstractFragment beforeFragment = null;



    //если будем использовать анимацию появления фрагмена, в аргументы добавляется boolean isAnimation и значение GlobalValues
    public void openFragment(AbstractFragment fragment,int fragmentContainerId){
        if (GlobalValues.FRAGMENTS_HISTORY == null) GlobalValues.FRAGMENTS_HISTORY = new ArrayList<>();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        List<Fragment> fragments = fragmentManager.getFragments();
        boolean hasFragment = false;
        AbstractFragment foundFragment = null;
        if (fragments != null) {
            for (int i = 0; i < fragments.size(); i++) {
                Fragment currentFragment = fragments.get(i);
                if (currentFragment instanceof AbstractFragment) {
                    if (currentFragment.getClass().equals(fragment.getClass())) {
                        hasFragment = true;
                        foundFragment = (AbstractFragment) currentFragment;
                        break;
                    }
                }
            }

            if (hasFragment) {
                if (beforeFragment != null) transaction.hide(beforeFragment);
                transaction.show(foundFragment).addToBackStack(foundFragment.getTagName()).commit();
                GlobalValues.FRAGMENTS_HISTORY.add(foundFragment.getTagName());
                beforeFragment = foundFragment;
            } else {
                if (beforeFragment != null) transaction.hide(beforeFragment);
                transaction.add(fragmentContainerId, fragment, fragment.getTagName()).commit();
                GlobalValues.FRAGMENTS_HISTORY.add(fragment.getTagName());
                beforeFragment = fragment;
            }
        } else {
            if (beforeFragment != null) transaction.hide(beforeFragment);
            transaction.add(fragmentContainerId, fragment, fragment.getTagName()).commit();
            GlobalValues.FRAGMENTS_HISTORY.add(fragment.getTagName());
            beforeFragment = fragment;
        }

    }
}
