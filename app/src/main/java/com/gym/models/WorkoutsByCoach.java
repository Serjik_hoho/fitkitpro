package com.gym.models;

/**
 * Created by jagor on 9/21/2017.
 */

public class WorkoutsByCoach {

    private String title;
    private String note;
    private String coachName;

    public WorkoutsByCoach(String title, String note, String coachName) {
        this.title = title;
        this.note = note;
        this.coachName = coachName;
    }

    public String getTitle() {
        return title;
    }

    public String getNote() {
        return note;
    }

    public String getCoachName() {
        return coachName;
    }
}
