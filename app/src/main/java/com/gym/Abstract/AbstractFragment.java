package com.gym.Abstract;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

/**
 * Created by Serj on 04.09.2017.
 */

public class AbstractFragment extends Fragment{


    @Override
    public void onPause() {
        try {
            super.onPause();
        } catch (IllegalStateException e) {
            Log.e("Fixme", "Error pausing");
        }
    }

    @Override
    public void onDestroyView() {
        Fragment parentFragment = getParentFragment();
        FragmentManager manager;
        if (parentFragment != null) {
            // If parent is another fragment, then this fragment is nested
            manager = parentFragment.getChildFragmentManager();
        } else {
            // This fragment is placed into activity
            manager = getActivity().getSupportFragmentManager();
        }
        try {
            manager.beginTransaction().remove(this).commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            Log.e("Fixme", "Error closing");
        }

        super.onDestroyView();
    }

    public String getTagName() {
        return getClass().getSimpleName(); //+ System.identityHashCode(this);
    }
}
