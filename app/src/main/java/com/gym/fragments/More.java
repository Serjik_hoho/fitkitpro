package com.gym.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gym.Abstract.AbstractFragment;
import com.gym.R;

/**
 * Created by Serj on 04.09.2017.
 */

public class More extends AbstractFragment {
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more,container,false);
        return view;
    }
}
