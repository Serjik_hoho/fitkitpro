package com.gym.fragments;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gym.Abstract.AbstractFragment;
import com.gym.R;

/**
 * Created by Serj on 04.09.2017.
 */

public class Feed extends AbstractFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed,container,false);
        return view;
    }
}
