package com.gym.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.gym.Abstract.AbstractFragment;
import com.gym.MainActivity;
import com.gym.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serj on 04.09.2017.
 */

public class Workouts extends AbstractFragment{

    private ViewPager workoutsViewPager;
    private BottomNavigationView topWorkoutMenu;

    BottomNavigationView.OnNavigationItemSelectedListener TNVListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()){
                case R.id.workouts_by_coach:
                    workoutsViewPager.setCurrentItem(0);
                    break;
                case R.id.standart_workouts:
                    workoutsViewPager.setCurrentItem(1);
                    break;
                case R.id.personal_workouts:
                    workoutsViewPager.setCurrentItem(2);
                    break;
            }
            return true;
        }
    };

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_workouts,container,false);

        topWorkoutMenu = (BottomNavigationView) view.findViewById(R.id.top_workout_navigation);
        topWorkoutMenu.setOnNavigationItemSelectedListener(TNVListener);

        workoutsViewPager = (ViewPager) view.findViewById(R.id.workouts_view_pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        workoutsViewPager.setAdapter(adapter);
        return view;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter{

        private List<Fragment> fragments = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            fillAdapter();
        }

        public void fillAdapter(){
            WorkoutsByCoachFragment workoutsByCoachFragment = new WorkoutsByCoachFragment();
            TestStandartWorkoutsFragment testStandartWorkoutsFragment = new TestStandartWorkoutsFragment();
            TestPersonalWorkoutsFragment testPersonalWorkoutsFragment = new TestPersonalWorkoutsFragment();
            fragments.add(workoutsByCoachFragment);
            fragments.add(testStandartWorkoutsFragment);
            fragments.add(testPersonalWorkoutsFragment);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
