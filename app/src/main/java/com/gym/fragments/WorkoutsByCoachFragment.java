package com.gym.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.gym.Abstract.AbstractFragment;
import com.gym.MainActivity;
import com.gym.R;
import com.gym.models.WorkoutsByCoach;
import com.gym.rvAdapters.RVAdapterWorkoutsByCoach;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutsByCoachFragment extends AbstractFragment {


    private RecyclerView recyclerView;
    private ArrayList<WorkoutsByCoach> workouts;
    private WorkoutsByCoach workoutsByCoach;
    private MainActivity activity;

    private GestureDetector detector = new GestureDetector(getActivity(), new GestureDetector.OnGestureListener() {
        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }
    });

    public WorkoutsByCoachFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_workouts_by_coach, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.workouts_by_coach_list);

        workoutsByCoach = new WorkoutsByCoach("3 days split", "Monday (chest)", "Ilya Bunin");
        workouts = new ArrayList<>();
        workouts.add(workoutsByCoach);

        RVAdapterWorkoutsByCoach adapter = new RVAdapterWorkoutsByCoach(workouts, getContext());
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());

                if(child != null && detector.onTouchEvent(e)){
                    activity = (MainActivity) getActivity();
                    TestMyWorkoutsByCoach fragment = new TestMyWorkoutsByCoach();
                    activity.showFragment(fragment);
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        return view;
    }

}
