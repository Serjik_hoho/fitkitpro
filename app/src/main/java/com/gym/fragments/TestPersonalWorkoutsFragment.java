package com.gym.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gym.Abstract.AbstractFragment;
import com.gym.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestPersonalWorkoutsFragment extends AbstractFragment {


    public TestPersonalWorkoutsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test_personal_workouts, container, false);
    }

}
